//
//  VXBaseViewController.h
//  MoneyDemo
//
//  Created by zhangxin on 2019/10/18.
//  Copyright © 2019 zhangxin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VXBaseViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
