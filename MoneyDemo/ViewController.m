//
//  ViewController.m
//  MoneyDemo
//
//  Created by zhangxin on 2019/10/18.
//  Copyright © 2019 zhangxin. All rights reserved.
//

#import "ViewController.h"
#import "VXZHViewController.h"
#import "VXGHViewController.h"
#import "VXJHViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}


- (IBAction)zhBtnAction:(id)sender {
    UIButton *btn = (UIButton *)sender;
    NSInteger tag = btn.tag;
    NSInteger type = 1;
    if (tag == 100) {
        type = 2;
    }
    VXZHViewController *zhVC = [[VXZHViewController alloc] init];
    zhVC.title = @"类别明细";
    zhVC.type = type;
    [self.navigationController pushViewController:zhVC animated:YES];
}

- (IBAction)ghBtnAction:(id)sender {
    VXGHViewController *ghVC = [[VXGHViewController alloc] init];
    ghVC.title = @"";
    [self.navigationController pushViewController:ghVC animated:YES];
}

- (IBAction)jhBtnAction:(id)sender {
    VXJHViewController *JHVC = [[VXJHViewController alloc] init];
    JHVC.title = @"查询明细";
    [self.navigationController pushViewController:JHVC animated:YES];
}
@end
