//
//  VXZHViewController.m
//  MoneyDemo
//
//  Created by zhangxin on 2019/10/18.
//  Copyright © 2019 zhangxin. All rights reserved.
//

#import "VXZHViewController.h"
#import "VXZHTableViewCell.h"
#import "VXXHHeadView.h"
#import "Masonry.h"

@interface VXZHViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) VXXHHeadView *headView;
@property (nonatomic,strong) NSMutableArray *dataArray;
@end

@implementation VXZHViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self baseSetter];
    
   
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    for (UIView *targetView in self.navigationController.navigationBar.subviews) {
        [targetView removeFromSuperview];
    }
}
- (void)baseSetter {
    self.headView.frame = CGRectMake(0, 0, self.view.frame.size.width, 210);
    self.tableView.tableHeaderView = self.headView;
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    //dataArray
    self.dataArray = [[NSMutableArray alloc] init];
    if (self.type != 2) {
        for (NSInteger i = 0; i < 6; i ++) {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            if (i == 0) {
                [dict setValue:@"仅对年度收入和年度支出进行分析（本人招行卡内互转、信用卡还款、投资理财、未入账交易，不计入当年收入和支出)" forKey:@"name"];
                [dict setValue:@2 forKey:@"type"];
                
            } else {
                [dict setValue:@"网上代发代扣" forKey:@"name"];
                [dict setValue:@1 forKey:@"type"];
                [dict setValue:@"储蓄卡5325" forKey:@"card"];
                if (i == 1) {
                    [dict setValue:@"+¥ 14,687.26" forKey:@"money"];
                    [dict setValue:@"2019-04-10 10:42:22" forKey:@"date"];
                } else if (i == 2) {
                    [dict setValue:@"+¥ 14,684.06" forKey:@"money"];
                    [dict setValue:@"2019-03-08 14:57:31" forKey:@"date"];
                } else if (i == 3) {
                    [dict setValue:@"+¥ 14,696.53" forKey:@"money"];
                    [dict setValue:@"2019-02-01 09:15:00" forKey:@"date"];
                } else if (i == 4) {
                    [dict setValue:@"+¥ 14,610.99" forKey:@"money"];
                    [dict setValue:@"2019-02-01 09:14:00" forKey:@"date"];
                } else if (i == 5) {
                    [dict setValue:@"+¥ 14,690.57" forKey:@"money"];
                    [dict setValue:@"2019-01-10 15:47:10" forKey:@"date"];
                }
                
            }
            [self.dataArray addObject:dict];
            NSMutableDictionary *headInfo = [[NSMutableDictionary alloc] init];
            [headInfo setValue:@"¥60,150.41" forKey:@"money"];
            [headInfo setValue:@"2019年   共5笔" forKey:@"date"];
            self.headView.infoDic = headInfo;
        }
    } else {
        NSData *JSONData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"monryData1" ofType:@"json"]];
        NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingAllowFragments error:nil];
        NSArray *dataArray = [jsonDic objectForKey:@"data"];
        [self.dataArray addObjectsFromArray:dataArray];
        NSMutableDictionary *headInfo = [[NSMutableDictionary alloc] init];
        [headInfo setValue:@"¥161,568.01" forKey:@"money"];
        [headInfo setValue:@"2018年   共13笔" forKey:@"date"];
        self.headView.infoDic = headInfo;
    }
    
    [self.tableView reloadData];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor],
       NSFontAttributeName : [UIFont systemFontOfSize:16.f]}];
    [self setCustomBackBarItem];
}
- (void)setCustomBackBarItem{
    UIButton *btn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    btn.frame = CGRectMake(0, 0, 40, 40);
    [btn setTitle:@"" forState:(UIControlStateNormal)];
    [btn setImage:[UIImage imageNamed:@"pcobb_common_fullscreen_back_disabled"] forState:(UIControlStateNormal)];
    [btn addTarget:self action:@selector(leftBarButtonItemReturnAction) forControlEvents:(UIControlEventTouchUpInside)];
    UIBarButtonItem *leftItem0 = [[UIBarButtonItem alloc]initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = leftItem0;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    
    UIImageView *topImageView = [[UIImageView alloc] init];
    [topImageView setImage:[UIImage imageNamed:@"zh_2"]];
    topImageView.frame = CGRectMake(self.navigationController.navigationBar.frame.size.width - 81 - 40, 0, 81 * 0.7, 51 * 0.7);
    [self.navigationController.navigationBar addSubview:topImageView];
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setTitle:@"..." forState:0];
    rightBtn.frame = CGRectMake(self.navigationController.navigationBar.frame.size.width - 15 - 40, 0, 40, 40 * 0.7);
    [rightBtn setTitleColor:[UIColor blackColor] forState:0];
    rightBtn.titleLabel.font = [UIFont systemFontOfSize:20];
    [self.navigationController.navigationBar addSubview:rightBtn];
    
}
- (void)leftBarButtonItemReturnAction {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - tableViewDelegate and dataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VXZHTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CELL"];
    NSDictionary *infoDic = self.dataArray[indexPath.row];
    cell.infoDic = infoDic;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 66;
}
#pragma mark - getter
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerNib:[UINib nibWithNibName:@"VXZHTableViewCell" bundle:nil] forCellReuseIdentifier:@"CELL"];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        UIView *footerView = [[UIView alloc] init];
        footerView.backgroundColor = [UIColor colorWithRed:248/255.0 green:248/255.0 blue:248/255.0 alpha:1];
        _tableView.tableFooterView = footerView;
    }
    return _tableView;
}
- (VXXHHeadView *)headView {
    if (!_headView) {
        _headView = [[VXXHHeadView alloc] init];
    }
    return _headView;
}
@end
