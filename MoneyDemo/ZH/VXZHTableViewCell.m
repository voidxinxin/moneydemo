//
//  VXZHTableViewCell.m
//  MoneyDemo
//
//  Created by zhangxin on 2019/10/18.
//  Copyright © 2019 zhangxin. All rights reserved.
//

#import "VXZHTableViewCell.h"

@implementation VXZHTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}
- (void)setInfoDic:(NSDictionary *)infoDic {
    _infoDic = infoDic;
    NSInteger type = [[infoDic objectForKey:@"type"] integerValue];
    if (type == 1) {
        self.nameLabel.hidden = NO;
        self.dateLabel.hidden = NO;
        self.cardIdLabel.hidden = NO;
        self.moneyLabel.hidden = NO;
        self.contentLabel.hidden = YES;
        self.nameLabel.text = [infoDic objectForKey:@"name"];
        self.cardIdLabel.text = [infoDic objectForKey:@"card"];
        self.dateLabel.text = [infoDic objectForKey:@"date"];
        self.moneyLabel.text = [infoDic objectForKey:@"money"];
    } else if (type == 2){
        self.nameLabel.hidden = YES;
        self.dateLabel.hidden = YES;
        self.cardIdLabel.hidden = YES;
        self.moneyLabel.hidden = YES;
        self.contentLabel.hidden = NO;
        NSString *name = [infoDic objectForKey:@"name"];
        self.contentLabel.text = name;
    }
    
}

@end
