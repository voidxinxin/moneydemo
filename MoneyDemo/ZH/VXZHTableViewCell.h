//
//  VXZHTableViewCell.h
//  MoneyDemo
//
//  Created by zhangxin on 2019/10/18.
//  Copyright © 2019 zhangxin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VXZHTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *cardIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (nonatomic,strong) NSDictionary *infoDic;
@end

NS_ASSUME_NONNULL_END
