//
//  VXXHHeadView.m
//  MoneyDemo
//
//  Created by 张新 on 2019/10/18.
//  Copyright © 2019年 zhangxin. All rights reserved.
//

#import "VXXHHeadView.h"
#import "Masonry.h"

@implementation VXXHHeadView
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self addUI];
    }
    return self;
}
- (void)addUI {
    UIImageView *imageView = [[UIImageView alloc] init];
    [imageView setImage:[UIImage imageNamed:@"zh_1"]];
    self.imageView = imageView;
    [self addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@90);
        make.height.equalTo(@66);
        make.top.equalTo(self).offset(20);
        make.centerX.equalTo(self);
    }];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = [UIFont systemFontOfSize:15];
    titleLabel.textColor = [UIColor colorWithRed:74/255.0 green:74/255.0 blue:75/255.0 alpha:1];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = @"工资福利";
    self.titleLabel = titleLabel;
    [self addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.imageView.mas_bottom).offset(10);
        make.height.equalTo(@15);
        make.width.equalTo(@100);
        make.centerX.equalTo(self);
    }];
    
    UILabel *moneyLabel = [[UILabel alloc] init];
   // moneyLabel.text = @"¥60,150.41";
    moneyLabel.font = [UIFont systemFontOfSize:25 weight:UIFontWeightBold];
    moneyLabel.textAlignment = NSTextAlignmentCenter;
    moneyLabel.textColor = [UIColor colorWithRed: 81/255.0 green: 82/255.0 blue:82 / 255.0 alpha:1];
    [self addSubview:moneyLabel];
    self.moneyLabel = moneyLabel;
    [moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(15);
        make.right.equalTo(self).offset(-15);
        make.height.equalTo(@25);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(20);
    }];
    
    UILabel *dateLabel = [[UILabel alloc] init];
    //dateLabel.text = @"2019年   共5笔";
    dateLabel.font = [UIFont systemFontOfSize:13.f];
    dateLabel.textColor = [UIColor colorWithRed:152 / 255.0 green:152/255.0 blue:152 / 255.0 alpha:1];
    dateLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:dateLabel];
    self.dataLabel = dateLabel;
    [dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.moneyLabel.mas_bottom).offset(20);
        make.height.equalTo(@14);
        make.width.equalTo(@250);
        make.centerX.equalTo(self);
    }];
    
}

- (void)setInfoDic:(NSDictionary *)infoDic {
    _infoDic = infoDic;
    self.dataLabel.text = [infoDic objectForKey:@"date"];
    self.moneyLabel.text = [infoDic objectForKey:@"money"];
}
@end
