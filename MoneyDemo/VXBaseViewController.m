//
//  VXBaseViewController.m
//  MoneyDemo
//
//  Created by zhangxin on 2019/10/18.
//  Copyright © 2019 zhangxin. All rights reserved.
//

#import "VXBaseViewController.h"

@interface VXBaseViewController ()

@end

@implementation VXBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    UINavigationBar *navigationBar = self.navigationController.navigationBar;
    [navigationBar setBackgroundImage:[UIImage imageNamed:@"white.png"]
                       forBarPosition:UIBarPositionAny
                           barMetrics:UIBarMetricsDefault];
    [navigationBar setShadowImage:[UIImage new]];
}

@end
