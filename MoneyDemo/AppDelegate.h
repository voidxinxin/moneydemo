//
//  AppDelegate.h
//  MoneyDemo
//
//  Created by zhangxin on 2019/10/18.
//  Copyright © 2019 zhangxin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow * window;

@end

