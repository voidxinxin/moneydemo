//
//  GHSectionView.h
//  MoneyDemo
//
//  Created by 张新 on 2019/10/18.
//  Copyright © 2019年 zhangxin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GHSectionView : UIView
@property (nonatomic,strong) UILabel *titleLabel;
@property (nonatomic,strong) NSDictionary *infoDic;
@end

NS_ASSUME_NONNULL_END
