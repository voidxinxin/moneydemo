//
//  GHTableViewCell.m
//  MoneyDemo
//
//  Created by 张新 on 2019/10/18.
//  Copyright © 2019年 zhangxin. All rights reserved.
//

#import "GHTableViewCell.h"

@implementation GHTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setInfoDic:(NSDictionary *)infoDic {
    _infoDic = infoDic;
    NSInteger type = [[infoDic objectForKey:@"type"] integerValue];
    NSString *imageStr = type == 1 ? @"gh_1" : @"gh_2";
    [self.tagImageView setImage:[UIImage imageNamed:imageStr]];
    self.nameLabel.text = [infoDic objectForKey:@"name"];
    self.cardLabel.text = [infoDic objectForKey:@"card"];
    self.monryLabel.text = [infoDic objectForKey:@"money"];
}
@end
