//
//  GHSectionView.m
//  MoneyDemo
//
//  Created by 张新 on 2019/10/18.
//  Copyright © 2019年 zhangxin. All rights reserved.
//

#import "GHSectionView.h"
#import "Masonry.h"
@implementation GHSectionView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self addUI];
    }
    return self;
}
- (void)addUI {
    // 230 236 241
    self.backgroundColor = [UIColor colorWithRed:230 / 255.0 green:236/255.0 blue:241/255.0 alpha:1];
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"000";
    titleLabel.font = [UIFont systemFontOfSize:14.0f];
    titleLabel.textColor = [UIColor colorWithRed:129/255.0 green:129/255.0 blue:129/255.0 alpha:1];
    [self addSubview:titleLabel];
    self.titleLabel = titleLabel;
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(17);
        make.right.equalTo(self).offset(-15);
        make.height.equalTo(@20);
        make.centerY.equalTo(self).offset(7);
    }];
}
- (void)setInfoDic:(NSDictionary *)infoDic {
    _infoDic = infoDic;
    NSString *date = [infoDic objectForKey:@"date"];
    self.titleLabel.text = date;
    if ([date containsString:@"星期"]) {
         self.backgroundColor = [UIColor colorWithRed:230 / 255.0 green:236/255.0 blue:241/255.0 alpha:1];
        self.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        self.titleLabel.textColor = [UIColor colorWithRed:129/255.0 green:129/255.0 blue:129/255.0 alpha:1];
    } else {
         self.backgroundColor = [UIColor colorWithRed:245 / 255.0 green:245/255.0 blue:245/255.0 alpha:1];
        self.titleLabel.font = [UIFont systemFontOfSize:17];
        self.titleLabel.textColor = [UIColor blackColor];
    }
}
@end
