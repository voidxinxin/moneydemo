//
//  VXGHViewController.m
//  MoneyDemo
//
//  Created by 张新 on 2019/10/18.
//  Copyright © 2019年 zhangxin. All rights reserved.
//

#import "VXGHViewController.h"
#import "GHTableViewCell.h"
#import "GHHeadView.h"
#import "Masonry.h"
#import "GHSectionView.h"

@interface VXGHViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) GHHeadView *headView;
@property (nonatomic,strong) NSArray *dataArray;
@end

@implementation VXGHViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self baseSetter];
    
    
    
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    for (UIView *targetView in self.navigationController.navigationBar.subviews) {
        [targetView removeFromSuperview];
    }
}
- (void)baseSetter {
    self.headView.frame = CGRectMake(0, 0, self.view.frame.size.width, 120);
    self.tableView.tableHeaderView = self.headView;
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    NSData *JSONData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"monryData" ofType:@"json"]];
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingAllowFragments error:nil];
    NSArray *dataArray = [jsonDic objectForKey:@"data"];
    if (dataArray.count > 2) {
        self.dataArray = @[dataArray.firstObject,dataArray[1]];
    }
    [self.tableView reloadData];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor],
                                                                      NSFontAttributeName : [UIFont systemFontOfSize:16.f]}];
    [self setCustomBackBarItem];
}
- (void)setCustomBackBarItem{
    UIButton *btn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    btn.frame = CGRectMake(0, 0, 87 * 0.6, 52 * 0.5 );
    [btn setTitle:@"" forState:(UIControlStateNormal)];
    [btn setImage:[UIImage imageNamed:@"zh_3"] forState:(UIControlStateNormal)];
    [btn addTarget:self action:@selector(leftBarButtonItemReturnAction) forControlEvents:(UIControlEventTouchUpInside)];
    UIBarButtonItem *leftItem0 = [[UIBarButtonItem alloc]initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = leftItem0;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
    
    UILabel * chatLabel = [[UILabel alloc] init];
    chatLabel.text = @"支出";
    chatLabel.font = [UIFont systemFontOfSize:15.0];
    chatLabel.textColor = [UIColor colorWithRed:155 / 255.0 green:166 /255.0 blue:180 /255.0 alpha:1];
    [self.navigationController.navigationBar addSubview:chatLabel];
    chatLabel.frame = CGRectMake(self.navigationController.navigationBar.frame.size.width * 0.5 - 70, 15, 50, 20);
    
    UILabel *enterLabel = [[UILabel alloc] init];
    enterLabel.text = @"收入";
    enterLabel.textColor = [UIColor colorWithRed:83 / 255.0 green:90 /255.0 blue:100 /255.0 alpha:1];
    enterLabel.font = [UIFont systemFontOfSize:16.f];
    
    [self.navigationController.navigationBar addSubview:enterLabel];
    enterLabel.frame = CGRectMake(self.navigationController.navigationBar.frame.size.width * 0.5 + 30, 15, 50, 20);
    
    UIView *line = [[UIView alloc] init];
    line.backgroundColor = [UIColor colorWithRed:27 / 255.0 green:132 /255.0 blue:211 /255.0 alpha:1];
     [self.navigationController.navigationBar addSubview:line];
    line.frame = CGRectMake(self.navigationController.navigationBar.frame.size.width * 0.5 + 34, 43, 25, 2);
    
    
}
- (void)leftBarButtonItemReturnAction {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - tableViewDelegate and dataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSDictionary *infoDic = self.dataArray[section];
    NSArray *array = [infoDic objectForKey:@"dataArray"];
    return array.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    GHTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CELL"];
    NSDictionary *infoDic = self.dataArray[indexPath.section];
    NSArray *array = [infoDic objectForKey:@"dataArray"];
    cell.infoDic = array[indexPath.row];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 66;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    GHSectionView *sectionView = [[GHSectionView alloc] init];
    NSDictionary *infoDic = self.dataArray[section];
    sectionView.infoDic = infoDic;
    return sectionView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 42;
}

#pragma mark - getter
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerNib:[UINib nibWithNibName:@"GHTableViewCell" bundle:nil] forCellReuseIdentifier:@"CELL"];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        UIView *footerView = [[UIView alloc] init];
        footerView.backgroundColor = [UIColor colorWithRed:248/255.0 green:248/255.0 blue:248/255.0 alpha:1];
        _tableView.sectionFooterHeight = 0.1;
        _tableView.tableFooterView = footerView;
    }
    return _tableView;
}
- (GHHeadView *)headView {
    if (!_headView) {
        _headView = [[GHHeadView alloc] init];
    }
    return _headView;
}
@end
