//
//  GHHeadView.m
//  MoneyDemo
//
//  Created by 张新 on 2019/10/18.
//  Copyright © 2019年 zhangxin. All rights reserved.
//

#import "GHHeadView.h"
#import "Masonry.h"
@implementation GHHeadView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self addUI];
    }
    return self;
}
- (void)addUI {
    self.backgroundColor = [UIColor whiteColor];
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = [UIFont systemFontOfSize:18];
    titleLabel.textColor = [UIColor colorWithRed:74/255.0 green:74/255.0 blue:75/255.0 alpha:1];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = @"2019年总收入";
    self.titleLabel = titleLabel;
    [self addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(30);
        make.height.equalTo(@18);
        make.width.equalTo(@200);
        make.centerX.equalTo(self);
    }];
    
    UILabel *moneyLabel = [[UILabel alloc] init];
    moneyLabel.text = @"+35,000.00";
    moneyLabel.font = [UIFont systemFontOfSize:27.0];
    moneyLabel.textAlignment = NSTextAlignmentCenter;
    moneyLabel.textColor = [UIColor colorWithRed: 81/255.0 green: 82/255.0 blue:82 / 255.0 alpha:1];
    [self addSubview:moneyLabel];
    self.moneyLabel = moneyLabel;
    [moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(15);
        make.right.equalTo(self).offset(-15);
        make.height.equalTo(@30);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(20);
    }];
    
}
@end
