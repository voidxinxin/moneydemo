//
//  VXJHTableViewCell.h
//  MoneyDemo
//
//  Created by 张新 on 2019/10/19.
//  Copyright © 2019年 zhangxin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VXJHTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *weekLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel1;
@property (weak, nonatomic) IBOutlet UILabel *enterLabel1;
@property (weak, nonatomic) IBOutlet UILabel *nameLabe2;
@property (weak, nonatomic) IBOutlet UILabel *enterLabel2;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel3;
@property (weak, nonatomic) IBOutlet UILabel *enterLabel3;
@property (weak, nonatomic) IBOutlet UILabel *monenyLabel1;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel2;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel3;
@property (nonatomic,strong) NSDictionary *infoDic;
@end

NS_ASSUME_NONNULL_END
