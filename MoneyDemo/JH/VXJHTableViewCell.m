//
//  VXJHTableViewCell.m
//  MoneyDemo
//
//  Created by 张新 on 2019/10/19.
//  Copyright © 2019年 zhangxin. All rights reserved.
//

#import "VXJHTableViewCell.h"

@implementation VXJHTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}
- (void)setInfoDic:(NSDictionary *)infoDic {
    _infoDic = infoDic;
    self.dateLabel.text = [infoDic objectForKey:@"date"];
    self.weekLabel.text = [infoDic objectForKey:@"week"];
    self.nameLabel1.text = [infoDic objectForKey:@"name"];
    self.enterLabel1.text = [infoDic objectForKey:@"card"];
    self.monenyLabel1.text = [infoDic objectForKey:@"money"];
    
    self.nameLabe2.text = [infoDic objectForKey:@"name2"];
    self.enterLabel2.text = [infoDic objectForKey:@"card2"];
    self.moneyLabel2.text = [infoDic objectForKey:@"money2"];
    
    self.nameLabel3.text = [infoDic objectForKey:@"name3"];
    self.enterLabel3.text = [infoDic objectForKey:@"card3"];
    self.moneyLabel3.text = [infoDic objectForKey:@"money3"];
    
}
@end
