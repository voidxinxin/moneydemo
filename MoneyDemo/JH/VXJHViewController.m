//
//  VXJHViewController.m
//  MoneyDemo
//
//  Created by 张新 on 2019/10/19.
//  Copyright © 2019年 zhangxin. All rights reserved.
//

#import "VXJHViewController.h"

#import "VXJHTableViewCell.h"
#import "GHHeadView.h"
#import "Masonry.h"
#import "GHSectionView.h"

@interface VXJHViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) GHHeadView *headView;
@property (nonatomic,strong) NSMutableArray *dataArray;
@end

@implementation VXJHViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self baseSetter];
    
    
    
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    for (UIView *targetView in self.navigationController.navigationBar.subviews) {
        [targetView removeFromSuperview];
    }
    
}
- (void)baseSetter {
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    NSData *JSONData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"monryData2" ofType:@"json"]];
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingAllowFragments error:nil];
    NSMutableArray *dataArray = [[jsonDic objectForKey:@"data"] mutableCopy];
    self.dataArray = dataArray;
    [self.tableView reloadData];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:97/255.0 green:116/255.0 blue:130/255.0 alpha:1],
                                                                      NSFontAttributeName : [UIFont systemFontOfSize:17.f]}];
    [self setCustomBackBarItem];
}
- (void)setCustomBackBarItem{
    UIButton *btn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    btn.frame = CGRectMake(0, 0, 40, 40);
    [btn setTitle:@"" forState:(UIControlStateNormal)];
    [btn setImage:[UIImage imageNamed:@"pcobb_common_fullscreen_back_disabled"] forState:(UIControlStateNormal)];
    [btn addTarget:self action:@selector(leftBarButtonItemReturnAction) forControlEvents:(UIControlEventTouchUpInside)];
    UIBarButtonItem *leftItem0 = [[UIBarButtonItem alloc]initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = leftItem0;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    UILabel *rightLabel = [[UILabel alloc] init];
    rightLabel.text = @"我的账户";
    rightLabel.font = [UIFont systemFontOfSize:16.f];
    rightLabel.textColor = [UIColor colorWithRed:100 / 255.0 green:119 / 255.0 blue:130 / 255.0 alpha:1];
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
    [self.navigationController.navigationBar addSubview:rightLabel];
    rightLabel.frame = CGRectMake(self.navigationController.navigationBar.frame.size.width - 15 - 80, 12, 80, 20);
    
   
    
    
}
- (void)leftBarButtonItemReturnAction {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - tableViewDelegate and dataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSDictionary *infoDic = self.dataArray[section];
    NSArray *array = [infoDic objectForKey:@"dataArray"];
    return array.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VXJHTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CELL"];
    NSDictionary *infoDic = self.dataArray[indexPath.section];
    NSArray *array = [infoDic objectForKey:@"dataArray"];
    cell.infoDic = array[indexPath.row];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 153;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    GHSectionView *sectionView = [[GHSectionView alloc] init];
    NSDictionary *infoDic = self.dataArray[section];
    sectionView.infoDic = infoDic;
    return sectionView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 42;
}

#pragma mark - getter
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerNib:[UINib nibWithNibName:@"VXJHTableViewCell" bundle:nil] forCellReuseIdentifier:@"CELL"];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        UIView *footerView = [[UIView alloc] init];
        footerView.backgroundColor = [UIColor colorWithRed:248/255.0 green:248/255.0 blue:248/255.0 alpha:1];
        _tableView.sectionFooterHeight = 0.1;
        _tableView.tableFooterView = footerView;
    }
    return _tableView;
}
- (GHHeadView *)headView {
    if (!_headView) {
        _headView = [[GHHeadView alloc] init];
    }
    return _headView;
}
@end
